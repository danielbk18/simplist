/* My custom JS */

/* global object to store variables common to the app */
var CategoryVar = {
    batchSize: 9,
    shown: 0,
    response: "",
};


/**
 * Creates the product gallery with the ProductList
 * delivered by the ajax call to Tray API
 * 
 */
function createProductGallery() {
    var params = {};

    //this is hidden in a h2, tray gives the category id when the page loads
    //with a django built-in {{ category.id }}
    params["category_id"] = jQuery("#categoryid").text();
    params["sort"] = "id_desc";
    params["limit"] = "50";
    params["available"] = "1";

    //asks for the list of products in that gallery
    jQuery.ajax({
        method: "GET",
        url: "/web_api/products/",
        data: params
    }).done(function(response, textStatus, jqXHR) {

        //console.log(response);
        //button will be needed
        if (response.paging.total > CategoryVar.batchSize) {
            jQuery("#load-more-btn").show();
        }

        //response.Products = response.Products.reverse();

        //set global var
        CategoryVar.response = response;
        CategoryVar.shown = 0;

        //work happens here
        console.log("work happens here")
        sortById(response);
        appendProducts(response);

    }).fail(function(jqXHR, status, errorThrown) {
        var response = $.parseJSON(jqXHR.responseText);
    });
}

/*
* Tray API is not sorting correctly, it is safer to do it manually
  so that newest products will show up first
*/ 
function sortById(response) {
  const products = response.Products;
  products.sort( (a,b) => (b.Product.id - a.Product.id));
  console.log("sortados");
  // for (const prod of products) {
  //   console.log(prod.Product.id)
  // } 

}

/**
 * Does the manual work of creating the product gallery  
 */
function appendProducts(response) {

    var totalNumOfProducts = response.paging.total;

    //something went wrong, leave and alert
    if (CategoryVar.shown >= totalNumOfProducts) {
        jQuery("#load-more-btn").hide();
        return;
    }

    //usually shows the batch size 
    var toShow = CategoryVar.batchSize;
    //if it's too small, just show what you have. also, hides load more btn
    if (totalNumOfProducts - CategoryVar.shown < CategoryVar.batchSize) {
        toShow = totalNumOfProducts - CategoryVar.shown;
        jQuery("#load-more-btn").hide();
    }


    //var productsToAppend = jQuery();
    //do the appending work after setting up the snippets
    for (i = CategoryVar.shown; i < CategoryVar.shown + toShow; i++) {
        var productSnippet = setupProduct(response.Products[i].Product)
            //productsToAppend.after(productSnippet);
        jQuery(".ps-product-column").last().after(productSnippet.hide().fadeIn(1000));
    }

    //jQuery(".ps-product-column").last().after(jQuery(productsToAppend).hide().fadeIn(2000));

    //updates to show value
    CategoryVar.shown += toShow;
}

/**
 * setup a single product snippet with the product data from the ajax call
 */
function setupProduct(productData) {

    /* clone the hidden template */
    productSnippet = jQuery(".ps-product-column").first().clone();
    productSnippet.show();

    //setup snippets: name, price, offer price, tags
    productSnippet.find(".ps-product__title").text(productData.name); //DEBUG
    productSnippet.find("img").attr("src", productData.ProductImage[0].https);
    //if has sale price, adds tag and shows crossed price
    if (productData.promotional_price > 0) {
        productSnippet.find(".ps-badge--sale-off").show();
        productSnippet.find(".ps-product__price").html("R$ " + productData.promotional_price + " <del> R$ " + productData.price + "</del>");
    } else {
        productSnippet.find(".ps-product__price").text("R$ " + productData.price);
    }
    //free shipping tag
    if (productData.additional_button == "1") {
        productSnippet.find(".ps-badge--new").show();
    }
    //unavailable tag
    if (productData.stock == "0") {
        console.log("found product with 0 stock");
        productSnippet.find(".ps-badge--unavailable").show();
    }
    //links
    productSnippet.find(".ps-product__shopping").attr("href", productData.url.https);
    productSnippet.find(".ps-product__title").attr("href", productData.url.https);

    return productSnippet;
}

/**
 * Adicionar produto ao carrinho
 */
function addToCart(productId) {

    /** GET AND VALIDATE INPUT  */
    var dataSession = jQuery("html").attr("data-session");
    //checks if there is a size selected, if not, message and leave
    if (jQuery(".current").length) {
        //li-id is dinamically generated with the variant id value
        var variantId = jQuery(".current").attr("id");
    } else {
        //if single size, erases variant id field and proceed
        if (jQuery("#single-size").length) {
            variantId = "";
        } else {
            jQuery("#error-msg").html("Selecione um tamanho").fadeIn(1500).fadeOut(1500);
            return;
        }
    }
    //checks if the quantity is reasonable
    var inputForm = jQuery(".form-group--number > .form-control");
    var quantity = Number(inputForm.val());

    //the stock is saved in the link id, generated dinamically
    /* I removed this because the stock is not valid data
    var stock = Number(jQuery(".current > a").attr("id"));
    if (quantity > stock) {
        jQuery("#error-msg").html("Quantidade acima do estoque").fadeIn(1500).fadeOut(1500);
        return;
    } */

    /** SENDS THE REQUEST AND GOES TO CART PAGE */
    jQuery.ajax({
        method: "POST",
        url: "/web_api/cart/",
        contentType: "application/json; charset=utf-8",
        data: '{"Cart":{"session_id":"' + dataSession + '","product_id":"' + productId + '","quantity":"' + quantity + '","variant_id":"' + variantId + '"}}'
    }).done(function(response, textStatus, jqXHR) {
        //console.log(response);
        //goes to cart
        window.location.href = response.cart_url;
    }).fail(function(jqXHR, status, errorThrown) {
        var response = jQuery.parseJSON(jqXHR.responseText);
        console.log(response);
        console.log("code eh", response.code);
        //deixa a tray verificar se o estoque é suficiente ou não
        if (response.code == 400) {
            console.log("entrei aqui");
            jQuery("#error-msg").html(response.causes).fadeIn(500).fadeOut(2000);
        }
    });
}
/**
 * Change the selected size
 */
function selectSize(size) {
    jQuery(".current").removeClass("current");
    //if not unavailable, do sth
    if (!jQuery(size).hasClass("unavailable")) {
        jQuery(size).addClass("current");
    }
}

/**
 * Adds the quantity to buy until maximum stock for that size
 */
function addQuantity(btn) {
    var inputForm = jQuery(".form-group--number > .form-control");
    var qtd = Number(inputForm.val());
    qtd += 1;
    inputForm.val(qtd);
}

/**
 * Subtracts the quantity to buy until 1
 */
function subtractQuantity(btn) {
    var inputForm = jQuery(".form-group--number > .form-control");
    var qtd = Number(inputForm.val());
    if (qtd <= 1) {
        qtd = 1;
        return;
    }
    qtd -= 1;
    inputForm.val(qtd);
}


function setupSearchListener() {
    const targetNode = jQuery(".ps-form-search--2")
}

function makeLoginImmutable() {

    var target = document.getElementById('header-immut');
    //set observer to track style changes
    new MutationObserver(callback).observe(target, { attributes: true, attributeOldValue: true, attributeFilter: ['style'] });

    //everytime style is changed, change it back to original and reattach the observer
    function callback(mutations, observer) {
        var target = mutations[0].target;
        //avoid infinite loop
        observer.disconnect();
        mutation = mutations[0];
        mutations.forEach(function(mutation) {
            target.setAttribute(mutation.attributeName, mutation.oldValue);
        });
        observer.observe(target, { attributes: true, attributeOldValue: true, attributeFilter: ['style'] });
    }
}

function getProductVariants() {

    var id = jQuery(".product-id-tray").attr("id");
    var variantsDict = {}

    params = {};
    params["attrs"] = "Product.Variant";

    $.ajax({
        method: "GET",
        url: "/web_api/products/" + id,
        data: params
    }).done(function(response, textStatus, jqXHR) {

        var numVariants = response.Product.Variant.length;

        if (numVariants == 0) {
            jQuery(".ps-product__size").append('<span id="single-size">Único</span><br>"');
            return;
        }

        for (i = 0; i < numVariants; i++) {
            var varId = response.Product.Variant[i].id;

            //setup html in page, so that other ajax can update them later: avoids the wrong order
            var html = '<li id="' + varId + '"><a id="" href="#"><span></span></a></li>';
            jQuery(".ps-product__size").append(html);

            var params = {};
            params["sort"] = "id_desc";
            params["attr"] = "Variant.id,Variant.Sku,Variant.available";

            $.ajax({
                method: "GET",
                url: "/web_api/variants/" + varId,
                data: params
            }).done(function(resp, textStatus, jqXHR) {
                var id = resp.Variant.id;
                var size = resp.Variant.Sku[0]["value"];
                var stock = Number(resp.Variant.available);

                //setup basics
                jQuery("#" + id + "> a").attr("id", stock);
                jQuery("#" + id + "> a > span").text(size);
                jQuery("#" + id).click(function(event) {
                    event.preventDefault();
                    selectSize(this);
                })

                //setup stock specific
                if (stock > 0) {
                    jQuery("#" + id + "> a").attr("id", stock);
                    jQuery("#" + id + "> a > span").text(size);
                } else {
                    jQuery("#" + id).addClass("unavailable");
                    jQuery("#" + id + "> a").attr("id", stock);
                    jQuery("#" + id + "> a > span").html("<del>" + size + "</del>");
                }
            }).fail(function(jqXHR, status, errorThrown) {
                var response = $.parseJSON(jqXHR.responseText);
                console.log(response);
            });
        }



    }).fail(function(jqXHR, status, errorThrown) {
        var response = $.parseJSON(jqXHR.responseText);
        console.log(response);
    });



}

/**
 * MAIN FUNCTION
 */
jQuery(document).ready(function main() {

    //for some reason $ was in conflict (typerror jQuery(..) null)
    //i'm using jQuery instead of $ to fix it

    //test debug

    /* title */
    document.getElementsByTagName('title')[0].innerHTML = "Simplist";

    /* category page */
    if (jQuery("#categoryid").length) {
        createProductGallery();

        jQuery("#load-more-btn").click(function(event) {
            event.preventDefault();
            appendProducts(CategoryVar.response);
        });
    }

    /* product page */
    if (jQuery("#btn-comprar").length) {
        //selects first size as active
        //jQuery(".ps-product__size li").first().addClass("current");

        //get variants from ajax because tray was not listing variants with stock 0
        getProductVariants();

        //binds the click function to the li obj
        jQuery(".ps-product__size li").click(function(event) {
            event.preventDefault();
            selectSize(this);
        });

        //bind the add quantity func to form
        jQuery(".form-group--number > .up").click(function(event) {
            event.preventDefault();
            addQuantity(this);
        });

        //binds the substract quantity func to form
        jQuery(".form-group--number > .down").click(function(event) {
            event.preventDefault();
            subtractQuantity(this);
        });

        //cleans up tray payment opt
        jQuery(".ps-product__rating > span > br").hide();

    }

    /* login page */
    if (jQuery(".caixa-login").length) {
        jQuery("input").addClass("form-control");
        jQuery("button").addClass("ps-btn ps-btn--black");
        jQuery("h3").addClass("ps-heading");
        jQuery(".caixa-login").addClass("col-lg-6 col-md-6 col-sm-12");
        jQuery(".caixa-cadastro").addClass("col-lg-6 col-md-6 col-sm-12");
        jQuery(".container-titulo-login").addClass("ps-breadcrumb ps-breadcrumb--2");
        jQuery(".titulo-login").addClass("ps-heading");
        jQuery(".carrinho-tabs").hide();
    }

    /* advanced search */
    if (jQuery("#Vitrine").length) {
        jQuery("#vitrine-catalogo .container3").addClass("ps-breadcrumb--2");
        jQuery("#Vitrine").addClass("container");
        jQuery("#div_erro").addClass("container");
        jQuery("button").addClass("ps-btn ps-btn--black");
        jQuery("#vitrine-catalog h1").addClass("ps-btn ps-btn--black");
        jQuery(".Seguro").hide();
        jQuery("#ProdBlock").hide();
        jQuery("#div_erro center").hide();
        var btn_url = jQuery("#btn-buscar-hidden").text();
        jQuery("input[name=imageField]").attr("src", btn_url);
        jQuery("input[name=imageField]").css("width", "130px");

    }

    if (jQuery(".search-results-title").length) {
        jQuery(".page-next a").addClass("ps-btn ps-btn--black");
        jQuery(".page-prev a").addClass("ps-btn ps-btn--black");
    }

    /* register page */
    if (jQuery("#juridica").length) {
        jQuery(".Seguro").hide();
        jQuery("#CadastroAbas").hide();
        jQuery("script + .container3").addClass("ps-breadcrumb ps-breadcrumb--2");
        jQuery(".board > h1").addClass("ps-heading");
        jQuery("button").addClass("ps-btn ps-btn--black");
        jQuery(".obriga.red").html("asterisco");
        jQuery(".carrinho-tabs").hide();
        //jQuery(".txt-dados-pessoais > span").html('hehe<i class="fas fa-running"></i>');
    }

    /* faq / contato */
    if (jQuery("#lightwindow_overlay").length) {
        jQuery("#lightwindow_overlay").hide();
        jQuery("#lightwindow").hide();
    }

    /* paginas secundárias - segurança, contato, como comprar */
    if (!jQuery(".container3").hasClass("container")) {
        //jQuery(".container3").addClass("container");
        jQuery(".board").addClass("container");
        jQuery("h3").addClass("ps-heading");
        jQuery("h1").addClass("ps-heading");

    }

    /* maior gambiarra da vida */
    //makeLoginImmutable();

});