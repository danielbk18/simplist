
var revapi3,
    tpj = jQuery;

tpj(document).ready(function() {

    /* main page - assign the correct layers to prepare the slider gambiarra*/
    if (tpj("#rev_slider_3_1").length) {
        tpj("#rev_slider_3_1 li").each(function(slide_i, slide) {
            tpj(slide).attr("data-index", "rs-" + slide_i);

            tpj(slide).children("div").each(function(layer_i, layer) {
                tpj(layer).attr("id", "slide-" + slide_i + "-layer-" + layer_i);
            });
        });
    }

    if (tpj("#rev_slider_3_1").revolution == undefined) {
        revslider_showDoubleJqueryError("#rev_slider_3_1");
    } else {
        revapi3 = tpj("#rev_slider_3_1").show().revolution({
            sliderType: "standard",
            jsFileLocation: "",
            sliderLayout: "fullscreen",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                mouseScrollReverse: "default",
                onHoverStop: "off",
                arrows: {
                    style: "custom-nav",
                    enable: true,
                    hide_onmobile: true,
                    hide_onleave: true,
                    tmp: '',
                    left: {
                        h_align: "left",
                        v_align: "bottom",
                        h_offset: 20,
                        v_offset: 55
                    },
                    right: {
                        h_align: "right",
                        v_align: "bottom",
                        h_offset: 20,
                        v_offset: 55
                    }
                }
            },
            responsiveLevels: [1240, 1024, 778, 480],
            visibilityLevels: [1240, 1024, 778, 480],
            gridwidth: [1170, 1024, 778, 480],
            gridheight: [868, 768, 960, 720],
            lazyType: "none",
            shadow: 0,
            spinner: "spinner0",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            fullScreenAutoWidth: "off",
            fullScreenAlignForce: "off",
            fullScreenOffsetContainer: "",
            fullScreenOffset: "",
            disableProgressBar: "on",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });
    }

}); /*ready*/
